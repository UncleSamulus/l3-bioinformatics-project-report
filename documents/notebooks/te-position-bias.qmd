---
title: Is there a detectable bias in Transposable Elements positioning ?
author: Samuel Ortion
date: 2023-04-18
updated: 2023-04-22
date-format: long
code-fold: true
bibliography: ../../references.bib
---

It has been already demonstrated in the literature that transposable elements are more likely to be located in non coding or intergenic sequences @silva_conserved_2003.

We will try to reproduce this kind of results using our generated data.

```{r, message=FALSE}
library(tidyverse)
library(RPostgreSQL)
library(DBI)
library(stringr)
library(plotrix)
library(knitr)
primarycolor <- "#4365e2" # blue
```

Just in case:
```bash
sudo dnf install postgresql-devel
```
```r
install.packages("RPostgreSQL")
```

## Chromosome distribution

We will try to reproduce figure 2 from @kaminker_transposable_2002.
Each chromosomes are represented by a line and dots are the position of the transposable elements.

```{r}
# Query data from sql database
m <- dbDriver("PostgreSQL")
db <- 'tenacious'
con <- dbConnect(m, dbname=db)
# Check if connection is ok
dbListTables(con) # We can see the tables \o/
# Get transposable elements coordinates
query <- "SELECT numero_chromosome, start_position, end_position, orientation FROM te_location"
te_location <- dbGetQuery(con, query)
chromosomes <- dbGetQuery(con, "SELECT numero_chromosome, taille_chromosome FROM chromosomes")
# Filter out mitochondrial chromosome and Y chromosome
chromosomes <- chromosomes %>% filter(numero_chromosome != 'MT') %>% filter(numero_chromosome != "Y") %>% arrange(numero_chromosome)
# head(te_location)
```


```{r, fig.width=10, fig.height=10, fig.align='center', message=FALSE}
#| fig-caption: "Transposable elements distribution on chromosomes"
plot_chrom_density <- function(te_coordinates, chromosomes) {
    width <- 1
    height <- 1
    max_length <- max(chromosomes$taille_chromosome)
    scale_x <- function(x) {
        x/max_length * width
    }
    # Plot chromosome as segment of propOrtional sizes
    plot.new()
    plot.window(xlim = c(-0.1, 1), ylim = c(0, 1))
    for (i in 1:nrow(chromosomes)) {
        y <- i/nrow(chromosomes) * height
        size <- scale_x(chromosomes$taille_chromosome[i])
        segments(x0 = 0, y0 = y, x1 = size, y1 = y, lwd = 5)
        # Draw a label for each chromosome
        text(x = -0.05, y = y - 0.01, labels = chromosomes$numero_chromosome[i], cex = 1.5, pos = 3)
        # Draw telomere position if chromosome is splitted
        if (grepl("R", chromosomes$numero_chromosome[i])) {
            draw.circle(x=0, y=y, radius=0.01, col="red")
        }
        if (grepl("L", chromosomes$numero_chromosome[i])) {
            draw.circle(x=size, y=y, radius=0.01, col="red")
        }
    }
    # Plot transposable elements
    coords = list(x=c(), y=c()) 
    for (i in 1:nrow(te_coordinates)) {
        chromosome <- te_coordinates$numero_chromosome[i]
        y <- which(chromosomes$numero_chromosome == chromosome)/nrow(chromosomes) * height - 0.01
        x <- scale_x(te_coordinates$start_position[i])
        coords$x <- c(coords$x, x)
        coords$y <- c(coords$y, y)
    }
    points(coords$x, jitter(coords$y), pch=20, cex=0.5)
}

plot_chrom_density(te_location, chromosomes)
title(main = "TE distribution on chromosomes, y-jittered for visibility")
# dev.print(png, "./documents/media/plots/te_distribution_chromosome.png", width = 10, height = 10, units = "in", res = 300)
```

Let's plot the same figure with the genes positions two.

```{r}
# Retrieve genes coordinates
query <- "SELECT numero_chromosome, start_position, end_position, orientation FROM gene_location"
genes_location <- dbGetQuery(con, query)
# head(genes_location)
```

```{r, fig.width=10, fig.height=10, fig.align='center', message=FALSE, warning=FALSE}
#| fig-caption: "Transposable elements distribution on chromosomes along with genes, centromere extremities are indicated by red dots"
plot_chrom_density_with_genes <- function(te_coordinates, genes_coordinates, chromosomes) {
    width <- 1
    height <- 1
    max_length <- max(chromosomes$taille_chromosome)
    scale_x <- function(x) {
        x/max_length * width
    }
    # Plot chromosome as segment of propOrtional sizes
    plot.new()
    plot.window(xlim = c(-0.1, 1), ylim = c(0, 1))
    for (i in 1:nrow(chromosomes)) {
        y <- i/(nrow(chromosomes)) * height
        size <- scale_x(chromosomes$taille_chromosome[i])
        segments(x0 = 0, y0 = y, x1 = size, y1 = y, lwd = 5)
        # Draw a label for each chromosome
        text(x = -0.05, y = y - 0.01, labels = chromosomes$numero_chromosome[i], cex = 1.5, pos = 3)
        # Draw telomere position if chromosome is splitted
        if (grepl("R", chromosomes$numero_chromosome[i])) {
            draw.circle(x=0, y=y, radius=0.01, col="red")
        }
        if (grepl("L", chromosomes$numero_chromosome[i])) {
            draw.circle(x=size, y=y, radius=0.01, col="red")
        }
    }
    # Plot transposable elements
    coords = list(x=c(), y=c()) 
    for (i in 1:nrow(te_coordinates)) {
        chromosome <- te_coordinates$numero_chromosome[i]
        y <- which(chromosomes$numero_chromosome == chromosome)/nrow(chromosomes) * height - 0.01
        x <- scale_x(te_coordinates$start_position[i])
        coords$x <- c(coords$x, x)
        coords$y <- c(coords$y, y)
    }
    points(coords$x, jitter(coords$y) + 0.05, pch=20, cex=0.5, legend = "TE")
    # Plot genes
    coords_genes = list(x=c(), y=c())
    for (i in 1:nrow(genes_coordinates)) {
        chromosome <- genes_coordinates$numero_chromosome[i]
        y <- which(chromosomes$numero_chromosome == chromosome)/(nrow(chromosomes)) * height - 0.01
        x <- scale_x(genes_coordinates$start_position[i])
        coords_genes$x <- c(coords$x, x)
        coords_genes$y <- c(coords$y, y)
    }
    points(coords_genes$x, jitter(coords_genes$y) - 0.025, pch=20, cex=0.5, col=primarycolor, legend = "Genes")
    legend("topright", legend = c("TE", "Genes"), pch = 20, col = c("black", primarycolor), cex = 1.5)
}
plot_chrom_density_with_genes(te_location, genes_location, chromosomes)
title(main = "TE and gene distributions on chromosomes")
# dev.print(png, "./documents/media/plots/te_distribution_chromosome_genes.png", width = 10, height = 10, units = "in", res = 300)
```

This shows that TE positions is correlated to genes positions: the more there are genes, the more there aire transposable elements.

We can also find quasi empty regions around centromeres (?).

```{r}

#' Coordinates density in region
#' Use sliding window to count density of TE along 3R chromosome
#' @param coordinates
#' @param window_size
#' @param window_step
density_in_region <- function(coordinates, window_size, window_step, max_coordinates = NULL, min_coordinates = 0) {
    if (is.null(max_coordinates)) {
        max_coordinates <- max(coordinates)
    }
    regions <- seq(min_coordinates, max_coordinates, window_step)
    densities <- rep(0, length(regions))
    i <- 1
    for (region in regions) {
        density <- sum(coordinates >= region & coordinates <= region + window_size)
        densities[i] <- density
        i <- i + 1
    }
    return(data.frame(region=regions, density=densities))
}

sliding_size <- 10e5
sliding_step <- 10e3
chromosome_numero <- "3R"
chromosome_length <- chromosomes %>% filter(numero_chromosome == chromosome_numero) %>% pull(taille_chromosome)
te_coordinates <- te_location %>% filter(numero_chromosome == chromosome_numero) %>% pull(start_position)
gene_coordinates <- genes_location %>% filter(numero_chromosome == chromosome_numero) %>% pull(start_position)
region_te_densities <- density_in_region(te_coordinates, sliding_size, sliding_step, min_coordinates = 0, max_coordinates = chromosome_length)
region_gene_densities <- density_in_region(gene_coordinates, sliding_size, sliding_step, min_coordinates = 0, max_coordinates = chromosome_length)
```


```{r, fig.width=10, fig.height=10, fig.align='center', message=FALSE, warning=FALSE}
#| fig-caption: "Transposable elements density along 3R chromosome"
#| fig-alt: "Transposable elements density along 3R chromosome"

ggplot(region_te_densities, aes(x=region, y=density)) +
    geom_line() +
    labs(x="Position along chromosome", y="Density of TE") +
    ggtitle(paste0(c("Transposable elements density along ", chromosome_numero, " chromosome (window size: ", sliding_size, ", step: ", sliding_step, ")"), collapse=""))
# ggsave("../media/plots/te_density_chromosome_3R.png", width = 10, height = 10, units = "in", dpi = 300)
```

```{r, fig.width=10, fig.height=10, fig.align='center', message=FALSE, warning=FALSE}
#| fig-caption: "Genes and TE densities along 3R chromosome"

ggplot() +
    geom_line(data=region_gene_densities, aes(x=region, y=density), col=primarycolor) +
    labs(x="Position along chromosome", y="Density of gene") +
    ggtitle(paste0(c("Genes density along 3R chromosome ", chromosome_numero, " (window size: ", sliding_size, ", step: ", sliding_step, ")"), collapse=""))
# ggsave("../media/plots/gene_density_chromosome_3R.png", width = 10, height = 10, units = "in", dpi = 300) 
```

```{r}
#| fig-caption: "Genes and TE densities along 3R chromosome"
#| fig-alt: "Genes and TE densities along 3R chromosome"
ggplot() +
    geom_line(data=region_gene_densities, aes(x=region, y=density, color="gene")) +
    geom_line(data=region_te_densities, aes(x=region, y=density, color="TE")) +
    labs(x="Position along chromosome", y="Density of gene") +
    ggtitle(paste0(c("Genes and TE densities along 3R chromosome ", chromosome_numero, " (window size: ", sliding_size, ", step: ", sliding_step, ")"), collapse=""))
# ggsave("../media/plots/gene_te_density_chromosome_3R.pdf", dpi = 300)
```

## Testing TE family gene distance hypotheses

*Hypothesis*: There exists a family $\mathcal{F}$, such that the distance between a TE and the nearest gene is smaller than the distance between a TE and the nearest gene of another family $\mathcal{F}'$.

Let's construct a table with the following columns:
- TE id
- Family name
- Distance to the nearest gene

```sql
CREATE FUNCTION distance_to_gene(te_start INTEGER, te_end INTEGER, gene_start INTEGER, gene_end INTEGER) RETURNS INTEGER AS $$
    DECLARE
        distance INTEGER;
    BEGIN
        IF te_start > gene_end THEN
            distance := te_start - gene_end;
        ELSIF te_end < gene_start THEN
            distance := gene_start - te_end;
        ELSE
            distance := 0;
        END IF;
        RETURN distance;
    END;
$$ LANGUAGE plpgsql;
```
```text
CREATE FUNCTION
```
```sql
SELECT te.numero_accession_te AS te_id, te.family_name AS family_name, distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position) AS distance 
FROM transposable_elements te, te_location te_loc, gene_location gene_loc
WHERE te.numero_accession_te = te_loc.numero_accession_te
AND te_loc.numero_chromosome = gene_loc.numero_chromosome
AND distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position) = ALL(
    SELECT MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc_2.start_position, gene_loc_2.end_position))
    FROM gene_location gene_loc_2
    WHERE te_loc.numero_chromosome = gene_loc_2.numero_chromosome
);
```
Which is damned slow...

Let's try to improve it:
```sql
SELECT te.numero_accession_te AS te_id, te.family_name AS family_name, MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position)) AS distance 
    FROM transposable_elements te, te_location te_loc, gene_location gene_loc
    WHERE te.numero_accession_te = te_loc.numero_accession_te
    AND te_loc.numero_chromosome = gene_loc.numero_chromosome
    GROUP BY te.numero_accession_te, te.family_name
;
```

To copy the result in a file:
```sql
\copy (
    SELECT te.numero_accession_te AS te_id, te.family_name AS family_name, MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position)) AS distance 
    FROM transposable_elements te, te_location te_loc, gene_location gene_loc
    WHERE te.numero_accession_te = te_loc.numero_accession_te
    AND te_loc.numero_chromosome = gene_loc.numero_chromosome
    GROUP BY te.numero_accession_te, te.family_name
) TO './data/out/tmp/te_gene_distance.csv' DELIMITER ';' CSV HEADER;
```

```csv
te_id;family_name;distance
FBti0063377;INE-1;0
FBti0019195;H;264
FBti0060947;invader2;10022
FBti0019160;F;12368
FBti0062368;3S18;0
FBti0064014;INE-1;1375
FBti0060604;1360;0
FBti0060124;INE-1;0
FBti0020235;R1A1;8134
```


```{r}
te_gene_distance <- read.csv("../../data/out/tmp/te_gene_distance.csv", sep=";", header=TRUE)
head(te_gene_distance)
```

Plot boxplots for our three families of interests:

```{r}
te_gene_distance_ours <- te_gene_distance %>% filter(family_name %in% c("HMS-Beagle", "BS", "hopper"))
```
```{r}
ggplot(te_gene_distance_ours, aes(x=family_name, y=distance)) + geom_boxplot() +
  ggtitle("Distance between TE and nearest gene for our three TE families") +
    xlab("TE family") + ylab("Distance (bp)")
```

$H_0$: $\mu_1 = \mu_2 = \mu_3$

$H_1$: $\exists i, j: \mu_i \neq \mu_j$

```{r}
anova(lm(distance ~ family_name, data=te_gene_distance_ours))
```

p-value below $\alpha = 0.05$, we can reject the null hypothesis. There seems to be TE families that are closer to genes than others.

I should maybe apply a correction for multiple testing (Bonferroni?).

## Is there a relation between TE copy conservation and distance to closest gene?

Let's retrieve the table from our database:
```sql
\copy (
    SELECT te.numero_accession_te AS te_id, te.family_name AS family_name, family.family_class, MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position)) AS distance, pident, coverage
    FROM transposable_elements te, te_location te_loc, gene_location gene_loc, blast_hits blast, families family
    WHERE te.numero_accession_te = te_loc.numero_accession_te
    AND family.family_name = te.family_name
    AND te_loc.numero_chromosome = gene_loc.numero_chromosome
    AND te.numero_accession_te = blast.numero_accession_te2
    GROUP BY te.numero_accession_te, te.family_name, family.family_class, pident, coverage
) TO './data/out/tmp/te_gene_distance_blast.csv' DELIMITER ';' CSV HEADER;
```

```text
COPY 2230
```

```{r}
te_gene_distance_blast <- read.csv("../../data/out/tmp/te_gene_distance_blast.csv", sep=";", header=TRUE)
head(te_gene_distance_blast)
```

```{r}
ggplot(te_gene_distance_blast, aes(y=pident, x=distance, col=family_class)) + geom_point() +
  ggtitle("Identity score against nearest gene distance") +
    xlab("Distance (bp)") + ylab("Blast pident") + 
    # Do not display legend for family_name
    theme(legend.position="none")
# ggsave("../../data/out/figures/te_gene_distance_blast_pident.pdf", width=10, height=6)
```

Let try again with `coverage` metric

```{r}
ggplot(te_gene_distance_blast, aes(y=coverage, x=distance, col=family_class)) + geom_point() +
  ggtitle("Coverage score against nearest gene distance") +
    ylab("Blast coverage") + xlab("Distance (bp)")
# ggsave("../media/plots/te_gene_distance_blast_coverage.pdf", width=10, height=6)
```

It seems that there is a lot of NAs in `family_class` column. 
```{r}
te_gene_distance_blast_filtered <- te_gene_distance_blast %>% filter(is.na(family_class))
head(te_gene_distance_blast_filtered)
```
*NB*: this no longer returns a result with below db update.

Here are the guilty TE families:
```bash
grep 'NA' ./data/out/db/families.tsv             
```
```text
INE-1   NA
Porto1  NA
Q       NA
Y       NA
```

Let's fix the `family_class` column:
```sql
UPDATE families
SET family_class = 'transposon'
WHERE family_name = 'INE-1';
UPDATE families 
SET family_class = 'no-LTR'
WHERE family_name = 'Porto1';
```

Further bibliography on Q and Y TE families should be done (however, they do not appear in our dataframe, so it remains to be done...)

Testing if the relation between TE copy conservation and distance to closest gene is significant:
```{r}
conservation_distance_lm  <- lm(coverage ~ distance, data=te_gene_distance_blast)
summary(conservation_distance_lm)
```

```{r}
ggplot(te_gene_distance_blast, aes(y=coverage, x=distance, col=family_class)) + geom_point() +
  ggtitle("Coverage score against nearest gene distance") +
    ylab("Blast coverage") + xlab("Distance (bp)") +
    geom_smooth(method=lm, se=FALSE)
```

This seems not to be a good looking model.

Let's try to fit a linear model with a quadratic term:
```{r}
conservation_distance_lm2  <- lm(coverage ~ distance + I(distance^2), data=te_gene_distance_blast)
summary(conservation_distance_lm2)
```

$R^2 = 0.006474$, which is not very good, even if $p$-value, below $0.0003$ seems to state that there is a relation between coverage and distance squared. 

Not very convincing.


Let's plot coverage histogram with distances bins of 1000bp:
```{r}
# | fig-label: distance-hist
# | fig-caption: Histogram of distance to nearest gene distribution 
ggplot(te_gene_distance, aes(x=distance)) + geom_histogram(binwidth = 1000, fill = primarycolor) +
    ggtitle("Histogram of distance to nearest gene") +
    xlab("Distance (bp)") + ylab("Count")
ggsave("../media/plots/blast-distance-hist.pdf", width=6, height=4)
```

```{r}
# | fig-label: coverage-hist
# | fig-caption: Histogram of coverage distribution
ggplot(te_gene_distance_blast, aes(x=coverage)) + geom_histogram(binwidth=0.025, fill = primarycolor) +
    ggtitle("Histogram of coverage") +
    xlab("Coverage") + ylab("Count")
# ggsave("../media/plots/blast-coverage-hist.pdf", width=6, height=4)
```


Both distributions looks quite similar.

Most TE copies are located close to a gene - less than 1000bp away.

Most TE copies have coverage score. The less the coverage, the less copies there are.

## Correlation coefficient between the two sliding window density curves

(on 2023-04-30)

```{r}
cor(region_te_densities$density, region_gene_densities$density, method="pearson")
```

The correlation coefficient is negligible.

```{r}
cor(te_gene_distance_blast$coverage, te_gene_distance_blast$distance)
```

Here again, the correlation coefficient is negligible.


## Does the orientation affect these metrics?
(On 2023-05-04)

From now on, we did not take into account the orientation of the TE copy and gene.


Let's fix this!

Query the same data, but with orientation taken into account:

```sql
\copy (
    SELECT te.numero_accession_te AS te_id, te.family_name AS family_name, family.family_class, MIN(distance_to_gene(te_loc.start_position, te_loc.end_position, gene_loc.start_position, gene_loc.end_position)) AS distance, pident, coverage
    FROM transposable_elements te, te_location te_loc, gene_location gene_loc, blast_hits blast, families family
    WHERE te.numero_accession_te = te_loc.numero_accession_te
    AND family.family_name = te.family_name
    AND te_loc.numero_chromosome = gene_loc.numero_chromosome
    AND te.numero_accession_te = blast.numero_accession_te2
    AND te_loc.orientation = gene_loc.orientation
    GROUP BY te.numero_accession_te, te.family_name, family.family_class, pident, coverage
) TO './data/out/tmp/te_gene_distance_blast_same_orientation.csv' DELIMITER ';' CSV HEADER;
```
```text
COPY 2230
```

```{r}
te_gene_distance_blast_orientation <- read.csv("../../data/out/tmp/te_gene_distance_blast_same_orientation.csv", sep=";", header=TRUE)
kable(head(te_gene_distance_blast_orientation))
```

```{r}
ggplot(te_gene_distance_blast_orientation, aes(y=pident, x=distance, col=family_class)) + geom_point() +
  ggtitle("Identity score against nearest gene distance (same orientation)") +
    xlab("Distance (bp)") + ylab("Blast pident") + 
    theme(legend.position="none")
# ggsave("../media/plots/te_gene_distance_blast_coverage_same_orientation.pdf", width=6, height=4)
```