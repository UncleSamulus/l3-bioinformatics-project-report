-- Create TE-nacious database schema

-- Create a table for chromosomes
CREATE TABLE IF NOT EXISTS chromosomes (
    numero_chromosome VARCHAR(2) NOT NULL,
    taille_chromosome INT NOT NULL,
    CONSTRAINT pk_chromosome PRIMARY KEY (numero_chromosome)
);

-- Create a table for genes
CREATE TABLE IF NOT EXISTS genes (
    numero_accession_gene VARCHAR(20) NOT NULL,
    gene_length INT NOT NULL,
    CONSTRAINT pk_gene PRIMARY KEY (numero_accession_gene)
);

-- Create a table for gene location
CREATE TABLE IF NOT EXISTS gene_location (
    numero_accession_gene VARCHAR(20) NOT NULL,
    numero_chromosome VARCHAR(2) NOT NULL,
    start_position INT NOT NULL,
    end_position INT NOT NULL,
    orientation VARCHAR(1) NOT NULL,
    CONSTRAINT pk_gene_location PRIMARY KEY (numero_accession_gene, numero_chromosome),
    CONSTRAINT fk_gene_location_gene FOREIGN KEY (numero_accession_gene) REFERENCES genes (numero_accession_gene),
    CONSTRAINT fk_gene_location_chromosome FOREIGN KEY (numero_chromosome) REFERENCES chromosomes (numero_chromosome)
);

-- Create a table for TE families
CREATE TABLE IF NOT EXISTS families (
    family_name VARCHAR(20) NOT NULL,
    family_class VARCHAR(20) NOT NULL,
    CONSTRAINT pk_family PRIMARY KEY (family_name)
);

-- Create a table for tranposable elements
CREATE TABLE IF NOT EXISTS transposable_elements (
    numero_accession_te VARCHAR(20) NOT NULL,
    family_name VARCHAR(20) NOT NULL,
    te_length INT NOT NULL,
    reference BOOLEAN NOT NULL DEFAULT false,
    CONSTRAINT pk_te PRIMARY KEY (numero_accession_te),
    CONSTRAINT fk_te_family FOREIGN KEY (family_name) REFERENCES families (family_name)
);


-- Create a table for TE location
CREATE TABLE IF NOT EXISTS te_location (
    numero_accession_te VARCHAR(20) NOT NULL,
    numero_chromosome VARCHAR(2) NOT NULL,
    start_position INT NOT NULL,
    end_position  INT NOT NULL,
    orientation VARCHAR(1) NOT NULL,
    CONSTRAINT pk_te_location PRIMARY KEY (numero_accession_te, numero_chromosome),
    CONSTRAINT fk_te_location_te FOREIGN KEY (numero_accession_te) REFERENCES transposable_elements (numero_accession_te),
    CONSTRAINT fk_te_location_chromosome FOREIGN KEY (numero_chromosome) REFERENCES chromosomes (numero_chromosome)
);

-- Create a table for TE vs TE blast HSPs
CREATE TABLE IF NOT EXISTS blast_hits (
    numero_accession_te1 VARCHAR(20) NOT NULL,
    numero_accession_te2 VARCHAR(20) NOT NULL,
    pident REAL NOT NULL,
    length INT NOT NULL,
    coverage REAL NOT NULL,
    evalue DOUBLE PRECISION NOT NULL,
    number_of_hits INT NOT NULL,
    CONSTRAINT pk_blast_hits PRIMARY KEY (numero_accession_te1, numero_accession_te2),
    CONSTRAINT fk_blast_hits_te1 FOREIGN KEY (numero_accession_te1) REFERENCES transposable_elements (numero_accession_te),
    CONSTRAINT fk_blast_hits_te2 FOREIGN KEY (numero_accession_te2) REFERENCES transposable_elements (numero_accession_te)
);
