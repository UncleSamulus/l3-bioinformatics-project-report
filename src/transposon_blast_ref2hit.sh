#!/bin/bash
# A script to perform blast analyzis from reference sequence, against a set of hits of targetted family
# Usage ./transposon_blast_ref2hit.sh <reference>

transposon_family="$1"

REFERENCE_FOLDER="data/references"
REFERENCE_FILE="liste_familles_ET_fichierref.txt"


# Get the reference file
REFERENCE_SEQ_FILE=$(grep -w "$transposon_family" "$REFERENCE_FOLDER/$REFERENCE_FILE" | cut -d' ' -f4)

# Check if the reference file exists
if [ ! -f "$REFERENCE_FOLDER/$REFERENCE_SEQ_FILE" ]; then
    echo "Reference file $REFERENCE_SEQ_FILE does not exist"
    exit 1
fi

# Get file format from GenBank, EMBL or FASTA
EXTENSION="${REFERENCE_SEQ_FILE##*.}"
if [[ $EXTENSION = "fasta" ]]; then
    FORMAT="FASTA"
fi
if grep -q "LOCUS" "$REFERENCE_FOLDER/$REFERENCE_SEQ_FILE"; then
    FORMAT="GenBank"
elif grep -q "ID" "$REFERENCE_FOLDER/$REFERENCE_SEQ_FILE"; then
    FORMAT="EMBL"
fi

# Convert to FASTA if necessary
if [ "$FORMAT" != "FASTA" ]; then
    filename_no_ext="${REFERENCE_SEQ_FILE%.*}"
    REFERENCES_SEQ_FILE_FASTA="$transposon_family.fasta"
    seqret "$REFERENCE_FOLDER/$REFERENCE_SEQ_FILE" "./data/out/align/$REFERENCES_SEQ_FILE_FASTA" -osformat2 fasta
elif [ "$FORMAT" = "FASTA" ]; then
    cp "$REFERENCE_FOLDER/$REFERENCE_SEQ_FILE" ./data/out/align/
    REFERENCES_SEQ_FILE_FASTA="$REFERENCE_SEQ_FILE"
fi

# Get the hits file
./src/extract_family_sequences.awk -v target="$transposon_family" "./data/dmel-all-transposon-r6.38.fasta" > "./data/out/align/$transposon_family-all.fasta"

# Perform blast
makeblastdb -in "./data/out/align/$transposon_family-all.fasta"  -parse_seqids -dbtype nucl -out "./data/out/align/db/$transposon_family"
blastn -query "./data/out/align/$transposon_family.fasta" -db "./data/out/align/db/$transposon_family" -out "./data/out/align/$transposon_family.blast" -outfmt 7 -num_threads 4